rebuild:
	@cd build && make

generate:
	@cd build && rm -rf * && CXX=clang++-10 CC=clang-10 cmake -DCMAKE_BUILD_TYPE=Release .. && make 

run:
	@cd build && ./main f ../configs/backtest/live_config.json ../configs/backtest/folly.json