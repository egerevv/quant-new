#include <trading/model/abstract_prediction.h>

#include <trading_tiny/data_loaders/market_data_loader.h>
#include <trading_tiny/config/tiny_config.h>
#include <trading/config/common.h>
#include <folly/json.h>

#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <string.h>

#include "prediction_factory.h"


using namespace trading;

int main(int argc, char* argv[]) {
    InitLogger(argc, argv, false);
    auto tiny_config = ParseTinyConfig(argv[2]);

    auto config = ReadJson(argv[3]);
    ProcessData(CraeteUserPrediction(config.at("name").getString(), config.at("params")), tiny_config);
    
    return 0;
}