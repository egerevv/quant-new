#pragma once

#include "models/rand/prediction.h"


inline std::unique_ptr<trading::IPrediction> CraeteUserPrediction(
	const std::string& name,
	const folly::dynamic& params
) {
    if (name == RandPrediction::Name()) {
        return std::make_unique<RandPrediction>(params);
    }

	return nullptr;
}
