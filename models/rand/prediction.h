#include <trading/model/abstract_prediction.h>
#include <trading/core/std/api_macro.h>
#include <trading_tiny/config/tiny_config.h>

#include <deque>
#include <random>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../prediction.h"


using namespace trading;


class RandPrediction : public Prediction {
public:
    RandPrediction(const folly::dynamic& object) {
        std::string pair = object.at("pair").getString();
        auto exchange_string = object.at("exchange").getString();
        auto exchange = trading::StringToExchange(exchange_string);

        instrument = GetInstrument(exchange, pair);
    }

    void OnTimer() override {
    }

    void OnTrade(const Trade& trade) override {
    } 

    void OnOrderbookUpdate(const OrderbookUpdate& update) override {
    }

    void SetOptimizationParams(const Vector<double>& params) override {
    }
    
    Signal GetSignal(const Instrument* instrument) override {    
        return Signal{signal, Vector<double>({0, 1, 2, 3})};
    }

    static std::string Name() {
        return "rand";
    }
};
