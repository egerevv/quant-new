#pragma once

#include <trading/model/abstract_prediction.h>
#include <trading/core/std/api_macro.h>
#include <trading_tiny/config/tiny_config.h>

#include <deque>
#include <random>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>


using namespace trading;


class Prediction : public IPrediction {
public:
    int signal;
    
    const Instrument* instrument;
};
