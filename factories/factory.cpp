#include <trading/model/abstract_prediction.h>
#include <trading/core/std/api_macro.h>
#include <trading_tiny/config/tiny_config.h>
#include <trading/config/common.h>
#include <folly/json.h>

#include "../prediction_factory.h"


using namespace trading;


API std::shared_ptr<IPrediction> GetPrediction(const TinyConfig& config, const Vector<String>& params) {
    auto config_folly = ReadJson(params[0]);
    return CraeteUserPrediction(config_folly.at("name").getString(), config_folly.at("params"));
}