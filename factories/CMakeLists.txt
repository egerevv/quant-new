project(factories)
cmake_minimum_required(VERSION 3.7)

find_package(Boost REQUIRED python38)
find_package(PythonInterp 3.8 REQUIRED)
find_package(PythonLibs 3.8 REQUIRED)

add_library(prediction SHARED 
    factory.cpp
)

target_include_directories(prediction PUBLIC ${PYTHON_INCLUDE_DIRS})
target_link_libraries(prediction ${PYTHON_LIBS} ${PYTHON_TINY_LIBS})
